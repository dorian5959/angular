import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormsModule, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthGuard } from '../auth.guard';
import { AuthServiceService } from '../auth-service.service';
import { Client } from '../client/client';
import { ClientService } from '../client/client-service.service';

@Component({
  selector: 'app-authenticate',
  templateUrl: './authenticate.component.html',
  styleUrls: ['./authenticate.component.css']
})
export class AuthenticateComponent implements OnInit {
  client: Client;
  private clientsUrl: string;
  form;

  constructor(private fb: FormBuilder,
    private myRoute: Router,
    private auth: AuthServiceService, private http: HttpClient, private clientService: ClientService) {

     this.client = new Client();
     this.form = fb.group({
      login: ['', [Validators.required]],
      password: ['', Validators.required]
    });
  }
  ngOnInit() {
  }

  login() {
   this.clientService.checkLogin(this.client).subscribe( res => {
     console.log(res);
     if (res) {
      this.auth.sendToken(this.form.value.login);
     }
    }, err => {
      console.log(err);
      this.myRoute.navigate(['home']);
    });
  }
}
