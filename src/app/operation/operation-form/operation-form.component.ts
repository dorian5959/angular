import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Operation } from '../operation';
import { OperationService } from '../operation-service.service';
import { Compte } from '../../compte/compte';
import { CompteService } from '../../compte/compte-service.service';

@Component({
  selector: 'app-operation-form',
  templateUrl: './operation-form.component.html',
  styleUrls: ['./operation-form.component.css']
})
export class OperationFormComponent implements OnInit {
  comptes: Compte[];
  operation: Operation;

  constructor(private route: ActivatedRoute, private router: Router, private compteService: CompteService, private operationService: OperationService) {
    this.operation = new Operation();
  }
 
  onSubmit(idCompte: number) {
    
    this.operationService.saveByCompteId(this.operation, idCompte).subscribe(result => this.gotoUserList());
  }
 
  gotoUserList() {
    this.router.navigate(['/operations']);
  }

  ngOnInit() {
    this.compteService.findAll().subscribe(data => {
      this.comptes = data;
    });
  }
}