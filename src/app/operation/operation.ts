import { Compte } from "../compte/compte";

export class Operation {
    id: number
    libelle: string
    date: Date
    compte: Compte
}

