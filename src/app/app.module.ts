import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { ReactiveFormsModule, FormBuilder, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';


import { AppComponent } from './app.component';
import { ClientListComponent } from './client/client-list/client-list.component';
import { CompteListComponent } from './compte/compte-list/compte-list.component';
import { OperationListComponent } from './operation/operation-list/operation-list.component';
import { ClientFormComponent } from './client/client-form/client-form.component';
import { ClientService } from './client/client-service.service';
import { CompteService } from './compte/compte-service.service';
import { OperationService } from './operation/operation-service.service';
import { CompteFormComponent } from './compte/compte-form/compte-form.component';
import { OperationFormComponent } from './operation/operation-form/operation-form.component';
import { AuthenticateComponent } from './authenticate/authenticate.component';
import { AuthGuard } from './auth.guard';
import { AuthServiceService } from './auth-service.service';



@NgModule({
  declarations: [
    AppComponent,
    ClientListComponent,
    CompteListComponent,
    OperationListComponent,
    ClientFormComponent,
    CompteFormComponent,
    OperationFormComponent,
    AuthenticateComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [ClientService, CompteService, OperationService, AuthServiceService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
