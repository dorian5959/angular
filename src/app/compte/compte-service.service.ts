import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Compte } from './compte';
import { Observable } from 'rxjs';

@Injectable()
export class CompteService {

  private comptesUrl: string;
 
  constructor(private http: HttpClient) {
    this.comptesUrl = 'http://localhost:8080/comptes';
  }
 
  public findAll(): Observable<Compte[]> {
    return this.http.get<Compte[]>(this.comptesUrl);
  }
 
  public save(compte: Compte) {
    return this.http.post<Compte>(this.comptesUrl, compte);
  }

  public saveByClientId(compte: Compte, idClient:number) {
    return this.http.post<Compte>(this.comptesUrl +'/' + idClient, compte);
  }

}
