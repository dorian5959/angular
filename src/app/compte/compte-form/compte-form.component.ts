import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Compte } from '../compte';
import { CompteService } from '../compte-service.service';
import { Client } from '../../client/client';
import { ClientService } from '../../client/client-service.service';

@Component({
  selector: 'app-compte-form',
  templateUrl: './compte-form.component.html',
  styleUrls: ['./compte-form.component.css']
})
export class CompteFormComponent implements OnInit{

  clients: Client[];
  compte: Compte;


  constructor(private route: ActivatedRoute, private router: Router, private compteService: CompteService, private clientService: ClientService) {
    this.compte = new Compte();
  }

  onSubmit(idCli: number) {
    this.compteService.saveByClientId(this.compte, idCli).subscribe(result => this.gotoUserList());
  }

  gotoUserList() {
    this.router.navigate(['/comptes']);
  }

  ngOnInit() {
    this.clientService.findAll().subscribe(data => {
      this.clients = data;
    });
  }
}
