import { Operation } from "../operation/operation";

export class Compte {
    id: number
    libelle: string
    solde: number
    operation: Operation[]
    taux: number
    seuil: number
}
