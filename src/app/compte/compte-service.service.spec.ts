import { TestBed, inject } from '@angular/core/testing';

import { CompteServiceService } from './compte-service.service';

describe('CompteServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CompteServiceService]
    });
  });

  it('should be created', inject([CompteServiceService], (service: CompteServiceService) => {
    expect(service).toBeTruthy();
  }));
});
