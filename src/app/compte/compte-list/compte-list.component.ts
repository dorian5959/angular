import { Component, OnInit } from '@angular/core';
import { Compte } from '../compte';
import { CompteService } from '../compte-service.service';

@Component({
  selector: 'app-compte-list',
  templateUrl: './compte-list.component.html',
  styleUrls: ['./compte-list.component.css']
})
export class CompteListComponent implements OnInit {

  comptes: Compte[];

  constructor(private compteService: CompteService) {
  }
 
  ngOnInit() {
    this.compteService.findAll().subscribe(data => {
      this.comptes = data;
    });
  }
}