import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClientListComponent } from './client/client-list/client-list.component';
import { CompteListComponent } from './compte/compte-list/compte-list.component';
import { OperationListComponent } from './operation/operation-list/operation-list.component';
import { ClientFormComponent } from './client/client-form/client-form.component';
import { CompteFormComponent } from './compte/compte-form/compte-form.component';
import { OperationFormComponent } from './operation/operation-form/operation-form.component';
import { AuthGuard } from './auth.guard';
import { AuthenticateComponent } from './authenticate/authenticate.component';

const routes: Routes = [
  { path: 'clients', component: ClientListComponent, canActivate: [AuthGuard] },
  { path: 'addclient', component: ClientFormComponent, canActivate: [AuthGuard] },
  { path: 'comptes', component: CompteListComponent, canActivate: [AuthGuard] },
  { path: 'addcompte', component: CompteFormComponent, canActivate: [AuthGuard] },
  { path: 'operations', component: OperationListComponent, canActivate: [AuthGuard] },
  { path: 'addoperation', component: OperationFormComponent, canActivate: [AuthGuard] },

  { path: 'home', component: AuthenticateComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
