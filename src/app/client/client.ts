import { Compte } from "../compte/compte";

export class Client {
    id: number;
  	login: String;
	  password: String;
	  nom: String;
    prenom: String;
    dateDeNaissance: Date;
    tabComptes: Map<Number, Compte>;
}
