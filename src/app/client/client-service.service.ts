import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Client } from './client';
import { Observable } from 'rxjs';

@Injectable()
export class ClientService {

  private clientsUrl: string;
  private clientUrl: string;

  constructor(private http: HttpClient) {
    this.clientsUrl = 'http://localhost:8080/clients';
    this.clientUrl = 'http://localhost:8080/login';
  }

  public findAll(): Observable<Client[]> {
    return this.http.get<Client[]>(this.clientsUrl);
  }

  public findById(idCli: number): Observable<Client> {
    return this.http.get<Client>(this.clientsUrl + '/' + idCli);
  }

  public save(client: Client) {
    return this.http.post<Client>(this.clientsUrl, client);
  }
  public checkLogin(cli: Client): Observable<Boolean> {
    return this.http.post<Boolean>(this.clientUrl, cli);
  }
}
